import React, { useState, useEffect } from 'react';

function VehicleModelsForm() {
    const [name, setName] = useState('');
    const [picture_url, setPictureUrl] = useState('');
    const [manufacturer_id, setManufacturerId] = useState('');
    const [manufacturers, setManufacturers] = useState([]);

    useEffect(() => {
      async function getManufacturers() {
        const url = 'http://localhost:8100/api/manufacturers/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setManufacturers(data.manufacturers);
        }
      }
      getManufacturers();
    }, []);

    async function handleSubmit(event) {
      event.preventDefault();
      const data = {
        name,
        picture_url,
        manufacturer_id,
      };

      const locationUrl = 'http://localhost:8100/api/models/';
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(locationUrl, fetchConfig);
      if(response.ok) {
        setName('');
        setPictureUrl('');
        setManufacturerId('');
      }
    }

    const handleChangeName = (event) => {
      const value = event.target.value;
      setName(value);
    }

    const handleChangePictureUrl = (event) => {
      const value = event.target.value;
      setPictureUrl(value);
    }

    const handleChangeManufacturerId = (event) => {
      const value = event.target.value;
      setManufacturerId(value);
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new vehicle model</h1>
            <form onSubmit={handleSubmit} id="create-vehicle-model-form">

              <div className="form-floating mb-3">
                <input onChange={handleChangeName} value={name} placeholder="Model Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChangePictureUrl} value={picture_url} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">Picture URL</label>
              </div>
              <div className="mb-3">
                <select onChange={handleChangeManufacturerId} value={manufacturer_id} required name="manufacturer_id" id="manufacturer_id" className="form-select">
                  <option value="">Choose a manufacturer</option>
                  {manufacturers.map((manufacturer) => {
                    return (
                      <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
  }

  export default VehicleModelsForm;
