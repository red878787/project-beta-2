import React, { useState, useEffect } from 'react';

function SalesList() {
  const [sales, setSales] = useState([]);


    async function fetchSales() {
      const url = 'http://localhost:8090/api/sales/';
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setSales(data.sales);
      }
    }
    useEffect(() => {
        fetchSales();
  }, []);

  return (
    <>
      <h1>Sales</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson Name</th>
            <th>Employee ID</th>
            <th>Customer Name</th>
            <th>Automobile VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map(sale => (
            <tr key={sale.id}>
              <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
              <td>{sale.salesperson.employee_id}</td>
              <td>{sale.customer.first_name} {sale.customer.last_name}</td>
              <td>{sale.automobile.vin}</td>
              <td>{parseFloat(sale.price).toFixed(2)}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}

export default SalesList;
