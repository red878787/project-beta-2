import React, { useEffect, useState } from "react";


function TechniciansList() {
    const [technicians, setTechnicians] = useState([])
    const getTechnicians = async () => {
        const url = 'http://localhost:8080/api/technicians/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setTechnicians(data.technicians)
        }
    }
    useEffect(() => {
        getTechnicians()
    }, []);
    const remove = async (technician) => {
        const url = `http://localhost:8080/api/technicians/${technician.id}`
        const fetchConfig = {
            method: `DELETE`,
            headers: {
                'Content-type': 'application/json',
            },
        }
        try {
            const response = await fetch(url, fetchConfig)
            if (response.ok) {
                getTechnicians()
            }
        } catch (error) {
            console.log('error deleting')
        }
    }
    return (
        <>
            <h1>Technicians</h1>
            <table className='table table-striped'>

                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians.map((technician) => {
                        return (
                            <tr key={technician.id}>
                                <td>{technician.employee_id}</td>
                                <td>{technician.first_name}</td>
                                <td>{technician.last_name}</td>
                                <td><button onClick={() => remove(technician)}>remove</button></td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    );
}
export default TechniciansList;
