import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";

function ManufacturerList() {
    const [manufacturers,setManufacturers] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/'

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setManufacturers(data.manufacturers)
            }
        } catch(e) {
            console.error(e)
        }
    }

    useEffect(()=>{

    })

    useEffect(()=> {
        fetchData();
    },[]);

    const remove = async (manufacturer) => {
        try {
            const deleteId = manufacturer.id
            const url = `http://localhost:8100/api/manufacturers/${deleteId}`
            const fetchConfig = {
                method: "delete"
            };
            const response = await fetch(url, fetchConfig);
            if (response.ok) {
                console.log("manufacturer deleted")
                fetchData()
            }
    }   catch (e) {
            console.log(e)
        }

    };

    return (
        <>
          <NavLink className="nav-link" to="/create-manufacturer">Add another manufacturer</NavLink>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Manufacturer</th>
              </tr>
            </thead>
            <tbody>
              {manufacturers.map(manufacturer => (
                <tr key={manufacturer.id}>
                  <td>{manufacturer.name}</td>
                  <td><button onClick={()=> remove(manufacturer)}>Remove</button></td>
                </tr>
              ))}
            </tbody>
          </table>
        </>
      );
    }



export default ManufacturerList;
