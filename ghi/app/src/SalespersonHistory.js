import React, { useState, useEffect } from 'react';

function SalespersonHistory() {
    const [salespeople, setSalespeople] = useState([]);
    const [sales, setSales] = useState([]);
    const [selectedSalesperson, setSelectedSalesperson] = useState('');

    useEffect(() => {
        fetch('http://localhost:8090/api/salespeople/')
            .then(response => response.json())
            .then(data => setSalespeople(data.salespeople))
            .catch(error => console.error('Error fetching salespeople:', error));
    }, []);

    useEffect(() => {
        if (selectedSalesperson) {
            fetch(`http://localhost:8090/api/salesperson/${selectedSalesperson}/sales/`)
                .then(response => response.json())
                .then(data => { setSales(data);
                })
                .catch(error => console.error('Error fetching sales:', error));
        }
    }, [selectedSalesperson]);

    function handleSalespersonChange(event) {
        setSelectedSalesperson(event.target.value);
    }

    return (
        <div>
            <h1>Salesperson History</h1>
            <select onChange={handleSalespersonChange}>
                <option value="">Select salesperson...</option>
                {salespeople.map(salesperson => (
                    <option key={salesperson.id} value={salesperson.id}>
                        {salesperson.first_name} {salesperson.last_name}
                    </option>
                ))}
            </select>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Customer</th>
                        <th>Automobile VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales && sales.map(sale => (
                        <tr key={sale.id}>
                            <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{parseFloat(sale.price).toFixed(2)}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default SalespersonHistory;
