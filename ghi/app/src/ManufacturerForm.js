import React, { useState } from 'react';
function ManufacturerForm() {
    const [manufacturer, setManufacturer] = useState('');

    const handleManufacturerChange = (e) => {
        setManufacturer(e.target.value)
    }


    // const fetchData = async () => {
    //     const url = 'http://localhost:8100/api/manufacturers/'
    //     const response = await fetch(url)

    //     if (response.ok) {
    //         const data = await response.json();
    //     }
    // }


    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};
        data.name = manufacturer;



        const url = `http://localhost:8100/api/manufacturers/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig)


        if (response.ok) {

            const newManufacturer = await response.json();
            setManufacturer('');
        }
    }


    // useEffect(() => {
    //     fetchData();
    // },[])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new Manufacturer</h1>
                    <form id="create-manufacturer-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input onChange={handleManufacturerChange} placeholder="Manufacturer" value={manufacturer} required type="text" id="manufacturer" className="form-control" name="model" />
                            <label htmlFor="model">Manufacturer</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default ManufacturerForm
