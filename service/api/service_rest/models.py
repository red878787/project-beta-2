from django.db import models

# Create your models here.


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200, unique=True)
    sold = models.BooleanField(default=False)


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Appointment(models.Model):
    date_time = models.DateField()
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=200)
    vin = models.CharField(max_length=200)
    customer = models.CharField(max_length=200, null=True)

    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE
    )

    def canceled(self):
        self.status = "canceled"
        self.save()

    def finished(self):
        self.status = "Finished"
        self.save()

    def __str__(self):
        return f'{self.date_time} / {self.reason} / {self.customer}'
