from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.views import View
from decimal import Decimal
import json
from common.json import ModelEncoder
from .models import Salesperson, Customer, AutomobileVO, Sale

class AutomobileEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold"]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "address", "phone_number", "id"]

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = ["id", "first_name", "last_name", "employee_id"]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = ["id", "automobile", "salesperson", "customer", "price"]
    encoders = {
        "automobile": AutomobileEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
    }
    def default(self, o):
        if isinstance(o, Sale):
            return super().default(o)
        elif isinstance(o, Decimal):
            return str(o)
        else:
            return super().default(o)

class SalespersonSales(View):
    def get(self, request, id, *args, **kwargs):
        sales = Sale.objects.filter(salesperson_id=id)
        sales_data = []
        for sale in sales:
            sale_obj = {
                'id': sale.id,
                'price': sale.price,
                'automobile': {
                    'vin': sale.automobile.vin,
                    'sold': sale.automobile.sold,
                },
                'salesperson': {
                    'id': sale.salesperson.id,
                    'first_name': sale.salesperson.first_name,
                    'last_name': sale.salesperson.last_name,
                    'employee_id': sale.salesperson.employee_id,
                },
                'customer': {
                    'id': sale.customer.id,
                    'first_name': sale.customer.first_name,
                    'last_name': sale.customer.last_name,
                    'address': sale.customer.address,
                    'phone_number': sale.customer.phone_number,
                },
            }
            sales_data.append(sale_obj)
        return JsonResponse(sales_data, safe=False, encoder=SaleEncoder)

@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_view_salespeople(request, id):
    if request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(id=id)
            deleted = salesperson.delete()
            return JsonResponse({"Deleted": deleted[0] > 0})
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Salesperson does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_view_customer(request, id):
    if request.method == "DELETE":
        try:
            customer = Customer.objects.filter(id=id)
            deleted = customer.delete()
            return JsonResponse({"Deleted": deleted[0] > 0})
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder
        )
    else:
        content = json.loads(request.body)
        content["price"] = Decimal('%.2f' % float(content["price"]))
        try:
            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(employee_id=salesperson_id)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson ID"},
                status=400,
            )
        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer ID"},
                status=400,
            )
        try:
            vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid VIN"},
                status=400,
            )
        sales = Sale.objects.create(**content)
        return JsonResponse(
            sales,
            encoder=SaleEncoder,
            safe=False
        )

@require_http_methods(["DELETE"])
def api_view_sale(request, id):
    if request.method == "DELETE":
        try:
            sale = Sale.objects.filter(id=id)
            deleted = sale.delete()
            return JsonResponse({"Deleted": deleted[0] > 0})
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Sales record does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["PUT"])
def api_view_automobile(request, vin):
    if request.method == "PUT":
        try:
            automobile = AutomobileVO.objects.get(vin=vin)
            content = json.loads(request.body)
            sold_status = content.get('sold')
            if sold_status is not None:
                automobile.sold = sold_status
                automobile.save()
                return JsonResponse(
                    automobile,
                    encoder=AutomobileEncoder,
                    safe=False
                )
            else:
                return JsonResponse(
                    {"message": "Invalid data"},
                    status=400,
                )
        except AutomobileVO.DoesNotExist:
            response = JsonResponse({"message": "Automobile does not exist"})
            response.status_code = 404
            return response
